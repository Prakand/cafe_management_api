from flask import Flask, request

app = Flask(__name__)

items = [
    {
        "name": "Green Apple Mojito",
        "price": 250
    },
    {
        "name": "Veg Burger",
        "price": 60
    }
]


@app.get('/get-items')
def get_items():
    return {"Items": items}


@app.post('/add-item')
def add_item():
    request_data = request.get_json()
    items.append(request_data)
    return {"message": "Item added Successfully"}
